package capitulo1;

public class Usuario {

    private String nome;
    private Integer pontos;
    private boolean moderador;

    public Usuario(String nome, int pontos) {
        this.nome = nome;
        this.pontos = pontos;
    }
    
    public void tornaModerador() {
        moderador = true;
    }
    
    public void showNome() {
        System.out.println(nome);
    }
    
    public void showPontos() {
        System.out.println(pontos);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getPontos() {
        return pontos;
    }

    public void setPontos(Integer pontos) {
        this.pontos = pontos;
    }

    public boolean isModerador() {
        return moderador;
    }

    public void setModerador(boolean moderador) {
        this.moderador = moderador;
    }

    @Override
    public String toString() {
        return nome + ", " + pontos + ", " + moderador;
    }
}
