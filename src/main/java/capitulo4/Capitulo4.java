package capitulo4;

import capitulo1.Usuario;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Capitulo4 {

    public static void main(String[] args) {

        //// mostrando o metodo andThen
        List<Usuario> usuarios = Arrays.asList(new Usuario("Rubens", 100), new Usuario("Rodrigo", 150));

        Consumer<Usuario> c1 = u -> u.showNome();
        Consumer<Usuario> c2 = u -> u.showPontos();

        usuarios.forEach(c1.andThen(c2));
        
        //// mostrando o metodo removeIf com classe anonima

        List<Usuario> usuarioMt = new ArrayList();   // lista mutável
        usuarioMt.add(new Usuario("Rubens", 100));
        usuarioMt.add(new Usuario("Rodrigo", 150));
        
        usuarioMt.removeIf(new Predicate<Usuario>() {
            public boolean test(Usuario u) {
                return u.getPontos() > 140;
            }
        });

        //// mostrando o metodo removeIf com lambda
        
        usuarioMt.removeIf(u -> u.getPontos() > 140);
        
    }
}
