package capitulo8;

import capitulo1.Usuario;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Capitulo8 {
    
    public static void main(String[] args) {
        
        List<Usuario> usuarios = Arrays.asList(new Usuario("Rubens", 50), new Usuario("Rodrigo", 150), new Usuario("Gabriel", 100));
        
        //// ordenando com stream
        
        List<Usuario> usuariosOrdenadosEFiltrados = usuarios.stream()
                .filter(u -> u.getPontos() > 10)
                .sorted(Comparator.comparing(Usuario::getNome))
                .collect(Collectors.toList());
        
        //// somando todos os pontos
        
        int total = usuarios.stream()
                .mapToInt(Usuario::getPontos)
                .sum();
        
        //// somando utilizando reduce
        
        int total2 = usuarios.stream()
                .mapToInt(Usuario::getPontos)
                .reduce(0, (n1, n2) -> n1 + n2);
        
        //// transformando um stream em um iterator
        
        Iterator<Usuario> it = usuarios.stream()
                .filter(u -> u.getPontos() > 100)
                .iterator();
    }
}
