package capitulo7;

import capitulo1.Usuario;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Capitulo7 {

    public static void main(String[] args) {

        List<Usuario> usuarios = Arrays.asList(new Usuario("Rubens", 90), new Usuario("Gabriel", 120), new Usuario("Rodrigo", 150));

        //// filtrando com stream
        
        usuarios.stream()
                .filter(u -> u.getPontos() > 100)
                .forEach(System.out::println);
        
        //// obtendo a lista filtrada (menos funcional)
        
        List<Usuario> comMaisDe100 = new ArrayList();
        
        usuarios.stream()
                .filter(u -> u.getPontos() > 100)
                .forEach(comMaisDe100::add);
        
        //// obtendo a lista filtrada usando collect
        
        comMaisDe100 =  usuarios.stream()
                .filter(u -> u.getPontos() > 100)
                .collect(Collectors.toList());
        
        //// listando apenas o atributo pontos com map
        
        List<Integer> pontos = usuarios.stream()
                .map(Usuario::getPontos)
                .collect(Collectors.toList());
        
        pontos.forEach(System.out::println);
        
        //// mapToInt para eviar autoboxing
        
        usuarios.stream()
                .mapToInt(Usuario::getPontos)
                .boxed()
                .collect(Collectors.toList());
        
                
    }
}
