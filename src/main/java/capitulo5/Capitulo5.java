package capitulo5;

import capitulo1.Usuario;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Capitulo5 {
    
    public static void main(String[] args) {
        
        //// ordenando pelo modelo tradicional
        
        List<Usuario> usuarios = Arrays.asList(new Usuario("Rubens", 100), new Usuario("Rodrigo", 200), new Usuario("Gabriel", 150));
        
        Comparator<Usuario> comparator = new Comparator<Usuario>() {
            public int compare(Usuario u1, Usuario u2) {
                return u1.getNome().compareTo(u2.getNome());
            }
        };
        
        Collections.sort(usuarios, comparator);
        usuarios.forEach(u -> u.showNome());
        
        //// ordenando com lambda (ainda usando Collections.sort)
        
        Collections.sort(usuarios, (u1, u2) -> u1.getPontos().compareTo(u2.getPontos()));
        usuarios.forEach(u -> u.showPontos());
        
        //// novo metodo List.sort
        
        usuarios.sort((u1, u2) -> u1.getNome().compareTo(u2.getNome()));
        usuarios.forEach(u -> u.showNome());
        
        //// metodos staticos em interfaces - comparando wrapers
        
        List<String> nomes = Arrays.asList("Rubens", "Gabriel", "Rodrigo");
        nomes.sort(Comparator.naturalOrder());
        
    }
}
