package capitulo2;

import capitulo1.Usuario;
import java.util.function.Consumer;

public class Consumidor implements Consumer<Usuario> {

    public void accept(Usuario u) {
        u.showNome();
    }
}
