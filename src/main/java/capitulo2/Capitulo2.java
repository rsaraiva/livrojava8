package capitulo2;

import capitulo1.Usuario;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Capitulo2 {

    public static void main(String[] args) {

        //// criando lista com 3 usuarios
        List<Usuario> usuarios = Arrays.asList(
                new Usuario("Rubens", 10), new Usuario("Rodrigo", 20), new Usuario("Gabriel", 30));
        
        //// modelo tradicional de loop em lista
        for (Usuario u : usuarios) {
            u.showNome();
        }
        
        //// utilizando o novo metodo forEach das collections, criando um Consumidor
        usuarios.forEach(new Consumidor());
        
        //// com classe anonima (bem verboso)
        usuarios.forEach(new Consumer<Usuario>() {
            public void accept(Usuario u) {
                u.showNome();
            }
        });
        
        //// iniciando com lamda para implementar um consumer (ainda verboso)
        usuarios.forEach((Usuario u) -> { u.showNome(); });
        
        //// por conta da inferência, nao ha necessidade do tipo nem do parenteses
        //// com instrucoes de uma linha, nao sao necessarios chaves e ponto-virgula
        usuarios.forEach(u -> u.showNome());
    }
}
