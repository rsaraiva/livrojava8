package capitulo6;

import capitulo1.Usuario;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Capitulo6 {
    
    public static void main(String[] args) {
        
        //// method reference
        
        List<Usuario> usuarios = Arrays.asList(new Usuario("Rubens", 100), new Usuario("Gabriel", 120));
        
        Consumer<Usuario> tornaModerador = Usuario::tornaModerador;
        usuarios.forEach(tornaModerador);
        
        usuarios.forEach(Usuario::tornaModerador);
        
        //// method reference com comparators
        
        usuarios.forEach(u -> u.showNome());
        
        usuarios.forEach(Usuario::showNome);
        
    }
}
